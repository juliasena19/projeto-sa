function Cadastrar(){
    var nomeCadastro = document.getElementById("nomeCadastro").value;
    var email = document.getElementById("emailCadastro").value;
    var senhaCadastro = document.getElementById("senhaCadastro").value;
    // var confirmSenha = document.getElementById("confSenha").value;
    var plano = ""
    var peso = ""
    var altura = ""
    var imc = ""

    var inputao = document.querySelector('.dados-cadastro')
    var inputs = inputao.querySelectorAll('.input');
    var label = document.querySelectorAll('#erroCadastro');
    var send = true;

    clear()
    for (let i = 0; i < inputs.length; i++) {
        if (inputs[i].value == '') {
            label[i].innerHTML = "Preencha todos os campos"
            send = false;
        }
        if(inputs[3].value !== inputs[2].value){
            label[3].innerHTML = "As senhas devem ser iguais!"
            send = false
        }
    }
    if (send !== true) {
        return false;
    }

    function clear() {
        for (let i = 0; i < inputs.length; i++) {
            label[i].innerHTML = '';
        }
    }

    var cadastros = JSON.parse(localStorage.getItem("cadastro_usuario")) || []

   
    for(i=0; i < cadastros.length; i++){
        var emailLocalStorage = cadastros[i].email
        if(email === emailLocalStorage){
            alert("Usuário já cadastrado!")
            return false
        }

    }
    cadastros.push({
        nome: nomeCadastro,
        senha: senhaCadastro,
        email: email,
        plano: plano,
        peso: peso,
        altura: altura,
        imc: imc
    })
    localStorage.setItem("cadastro_usuario", JSON.stringify(cadastros))

    alert("Cadastro Efetuado com sucesso!")
    window.location.href="../HTML/index.html"
}