// ABRIR MODAL PLANO BASIC
function PlanoBasico(mn) {
  let modal = document.getElementById(mn);

  if (typeof modal == "undefined" || modal === null) return;
  modal.style.display = "block";
  modal.style.zIndex = "999";

  // FUNCIONALIDADE SAIR DO MODAL CLICANDO FORA
  setTimeout(() => {
    document.addEventListener("click", handleClickOutside, false);
  }, 200);

  const handleClickOutside = (event) => {
    let overlay = document.getElementById("modalPlanoBasic");
    let modal = document.getElementById("modalPlanosContent");

    if (!modal.contains(event.target)) {
      modal.style.display = "block";
      overlay.style.display = "none";
      document.removeEventListener("click", handleClickOutside, false);
    }
  }
}
// ABRIR MODAL PLANO SILVER
function PlanoSilver(mn) {
  let modal = document.getElementById(mn);

  if (typeof modal == "undefined" || modal === null) return;
  modal.style.display = "block";
  modal.style.zIndex = "999";

  // FUNCIONALIDADE SAIR DO MODAL CLICANDO FORA
  setTimeout(() => {
    document.addEventListener("click", handleClickOutside, false);
  }, 200);

  const handleClickOutside = (event) => {
    let overlay = document.getElementById("modalPlanoSilver");
    let modal = document.getElementById("modalPlanoSilverContent");

    if (!modal.contains(event.target)) {
      modal.style.display = "block";
      overlay.style.display = "none";
      document.removeEventListener("click", handleClickOutside, false);
    }
  }
}
// ABRIR MODAL PLANO GOLD
function PlanoGold(mn) {
  let modal = document.getElementById(mn);

  if (typeof modal == "undefined" || modal === null) return;
  modal.style.display = "block";
  modal.style.zIndex = "999";

  // FUNCIONALIDADE SAIR DO MODAL CLICANDO FORA
  setTimeout(() => {
    document.addEventListener("click", handleClickOutside, false);
  }, 200);

  const handleClickOutside = (event) => {
    let overlay = document.getElementById("modalPlanoGold");
    let modal = document.getElementById("modalPlanoGoldContent");

    if (!modal.contains(event.target)) {
      modal.style.display = "block";
      overlay.style.display = "none";
      document.removeEventListener("click", handleClickOutside, false);
    }
  }
}
// COMPRAR PLANO BASIC
function ComprarPlanoBasic() {
  let checkboxPlano = document.getElementById('checkboxPlanos')
  let userPlano = JSON.parse(localStorage.getItem('cadastro_usuario'))
  let userPlanoLogado = JSON.parse(localStorage.getItem('logado'))
  let plano = "Basic"

  if (checkboxPlano.checked) {


    for (i = 0; i < userPlano.length; i++) {
      if (userPlanoLogado == userPlano[i].email) {


        console.log("cheguei aqui")
        userPlano[i].plano = plano;
        teste = userPlano
        localStorage.setItem('cadastro_usuario', JSON.stringify(teste))
        alert("Plano comprado com sucesso!")
        window.location.href = "../HTML/index.html"

      }
    }
  } else {
    alert("Você precisa estar de acordo com os termos")
  }
}
// COMPRAR PLANO SILVER
function ComprarPlanoSilver() {
  let checkboxPlano = document.getElementById('checkboxPlanoSilver')
  let userPlano = JSON.parse(localStorage.getItem('cadastro_usuario'))
  let userPlanoLogado = JSON.parse(localStorage.getItem('logado'))
  let plano = "Silver"

  if (checkboxPlano.checked) {


    for (i = 0; i < userPlano.length; i++) {
      if (userPlanoLogado == userPlano[i].email) {


        console.log("cheguei aqui")
        userPlano[i].plano = plano;
        teste = userPlano
        localStorage.setItem('cadastro_usuario', JSON.stringify(teste))
        alert("Plano comprado com sucesso!")
        window.location.href = "../HTML/index.html"

      }
    }
  } else {
    alert("Você precisa estar de acordo com os termos")
  }
}
// COMPRAR PLANO GOLD
function ComprarPlanoGold() {
  let checkboxPlano = document.getElementById('checkboxPlanoGold')
  let userPlano = JSON.parse(localStorage.getItem('cadastro_usuario'))
  let userPlanoLogado = JSON.parse(localStorage.getItem('logado'))
  let plano = "Gold"

  if (checkboxPlano.checked) {


    for (i = 0; i < userPlano.length; i++) {
      if (userPlanoLogado == userPlano[i].email) {


        console.log("cheguei aqui")
        userPlano[i].plano = plano;
        teste = userPlano
        localStorage.setItem('cadastro_usuario', JSON.stringify(teste))
        alert("Plano comprado com sucesso!")
        window.location.href = "../HTML/index.html"

      }
    }
  } else {
    alert("Você precisa estar de acordo com os termos")
  }
}

