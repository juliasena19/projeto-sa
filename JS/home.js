// const { Button } = require("bootstrap");

function Newsletter() {
  let validEmail = document.getElementById("input-email-footer").value;
  if (validEmail == "" || validEmail.indexOf("@") == -1) {
    document.getElementById("newsletterEnviado").innerHTML =
      "Insira um e-mail válido";
  } else {
    if (newsletter != "") {
      let erro = document.querySelector(".newsletter");
      erro.innerHTML = "E-mail enviado com Sucesso!";
      erro.style.fontSize = "20px";
      erro.style.color = "white";
      erro.style.paddingBottom = "130px";
      erro.style.paddingTop = "130px";
      newsletter = erro;
    }
  }
}
