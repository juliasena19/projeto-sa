//ABRINDO MODAL DE LOGIN
function OpenModal(mn) {
    var statusLocalStorage = localStorage.getItem('logado')
    if (statusLocalStorage == null) {
        let modalLoginD = document.getElementById(mn)

        if (typeof modalLoginD == 'undefined' || modalLoginD === null)
            return;
        modalLoginD.style.display = 'block';


        // FUNCIONALIDADE SAIR DO MODAL CLICANDO FORA 
        setTimeout(() => { document.addEventListener('click', handleClickOutside, false) }, 200)

        const handleClickOutside = (event) => {

            let overlay = document.getElementById("modalLogin");
            let modal = document.getElementById("modalLoginContent");

            if (!modal.contains(event.target)) {
                modal.style.display = 'block';
                overlay.style.display = 'none';
                document.removeEventListener('click', handleClickOutside, false);
            }
        }
    } else {
        window.location.href = "../HTML/espacoCliente.html"
    }
}

// PEGANDO DADOS DE LOGIN
function Login() {
    var objLocalStorage = JSON.parse(localStorage.getItem("cadastro_usuario"))

    var emailLogin = document.getElementById("emailLogin").value;
    var senhaLogin = document.getElementById("senhaLogin").value;

    //if (emailLogin == "" || senhaLogin == "") {

    var inputao = document.querySelector('.container-meio')
    var inputs = inputao.querySelectorAll('input');
    var label = document.querySelectorAll('#erro');
    var send = true;
    clear()
    for (let i = 0; i < inputs.length; i++) {
        if (inputs[i].value == '') {
            label[i].innerHTML = "Preencha todos os campos"
            send = false;
        }
    }
    if (send !== true) {
        return false;
    }

    function clear() {
        for (let i = 0; i < inputs.length; i++) {
            label[i].innerHTML = '';
        }
    } if (objLocalStorage == null || objLocalStorage == undefined) {
        document.getElementById("erro").innerHTML = "Usuário não existe"

    } else {

        for (i = 0; i < objLocalStorage.length; i++) {
            var objUsuario = objLocalStorage[i]

            if (emailLogin == objUsuario.email && senhaLogin == objUsuario.senha) {
                window.location.href = "../HTML/index.html"
                ValidaLogin(objUsuario)
                alert("login efetuado!")
                return false
            }
        }
        if (objLocalStorage) {
            document.getElementById("erro").innerHTML = "E-mail ou senha inválido";
        }
    }
}
function ValidaLogin(objUsuario) {
    localStorage.setItem('logado', JSON.stringify(objUsuario.email))
}