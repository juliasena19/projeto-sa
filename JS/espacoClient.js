function Sair() {

    window.location.href = "../HTML/index.html"
    localStorage.removeItem('logado')
}

var usuarioLogado = DadosUsuarios()
// IMPRIMINDO NOME EMAIL E PLANO NA TELA
document.getElementById('nomeLogado').innerHTML = usuarioLogado.nome
document.getElementById('emailLogado').innerHTML = usuarioLogado.email
document.getElementById('planoAtual').innerHTML = usuarioLogado.plano
// IMPRIMINDO DADOS IMC NA TELA
document.getElementById('imcUsuario').innerHTML = usuarioLogado.imc


function EditaSenhaUser() {

    let senhaUsuario = document.getElementById("senhaNova").value
    let emailUsuario = document.getElementById("confirmaEmail")
    let user = JSON.parse(localStorage.getItem('cadastro_usuario'))



    for (i = 0; i < user.length; i++) {
        if (user[i].email == emailUsuario.value) {
            user[i].senha = senhaUsuario
            senha = user
            localStorage.setItem('cadastro_usuario', JSON.stringify(senha))
            alert("Senha alterada com sucesso!")
        }
    }
}

function ExcluirUsuario() {

    let userRegistro = JSON.parse(localStorage.getItem('cadastro_usuario'))
    let userLogado = document.getElementById('buttonDelete').value
    userLogado = JSON.parse(localStorage.getItem('logado'))
    let indice = null
    
    for(i = 0; i < userRegistro.length; i++){
      
    if(userLogado == userRegistro[i].email ){
      
           indice = i;
        }         
    } 
    if(confirm("Quer realmente apagar esse usuario?")){            
      userRegistro.splice(indice, 1)   
      localStorage.setItem('cadastro_usuario', JSON.stringify(userRegistro))
      alert("Usuario excluido!")
      window.location.href = "../HTML/index.html"
      localStorage.removeItem('logado')
  }
}

//MODAL EDITAR DADOS
function EditarDados() {
    let modalEdit = document.getElementById('modalEditarDados');

    if (typeof modalEdit == "undefined" || modalEdit === null) return;
    modalEdit.style.display = "block";
    modalEdit.style.zIndex = "99999";

    setTimeout(() => {
        document.addEventListener("click", handleClickOutside, false);
      }, 200);
    
      const handleClickOutside = (event) => {
        let overlay = document.getElementById("modalEditarDados");
        let modal = document.getElementById("modalEditarDadosContent");
    
        if (!modal.contains(event.target)) {
          modal.style.display = "block";
          overlay.style.display = "none";
          document.removeEventListener("click", handleClickOutside, false);
        }
      }

      function ModalCalculoImc(mn) {
        let modal = document.getElementById(mn);
      
        if (typeof modal == "undefined" || modal === null) return;
        modal.style.display = "block";
        modal.style.zIndex = "999";
      
        // FUNCIONALIDADE SAIR DO MODAL CLICANDO FORA
        setTimeout(() => {
          document.addEventListener("click", handleClickOutside, false);
        }, 200);
      
        const handleClickOutside = (event) => {
          let overlay = document.getElementById("modalMeusDadosImc");
          let modal = document.getElementById("modalMeusDadosImcContent");
      
          if (!modal.contains(event.target)) {
            modal.style.display = "block";
            overlay.style.display = "none";
            document.removeEventListener("click", handleClickOutside, false);
          }
        }
      }


}
function OpenModalImc(mn){
        let modalImc = document.getElementById(mn)

        if (typeof modalImc == 'undefined' || modalImc === null)
            return;
        modalImc.style.display = 'block';
        modalImc.style.zIndex = '99999'


        // FUNCIONALIDADE SAIR DO MODAL CLICANDO FORA 
        setTimeout(() => { document.addEventListener('click', handleClickOutside, false) }, 200)

        const handleClickOutside = (event) => {

            let overlay = document.getElementById("modalMeusDadosImc");
            let modal = document.getElementById("modalMeusDadosImcContent");

            if (!modal.contains(event.target)) {
                modal.style.display = 'block';
                overlay.style.display = 'none';
                document.removeEventListener('click', handleClickOutside, false);
            }
        }
}



